import { OrderObject } from './OrderObject';
import { OrderDocs } from './OrderDocs';
import { OrderAccount } from './OrderAccount';
import { OrderDocument } from './OrderDocument';

export namespace response {
    export namespace order {
        export interface Get {
            debug: {
                0: OrderAccount[];
                1: OrderObject[];
                2: OrderDocs[];
            };
        }
    }
    export namespace orderObjects {
        export interface Get extends Array < OrderObject > {}
    }
    export namespace orderAccounts {
        export interface Get extends Array < OrderAccount > {}
    }
    export namespace orderDocuments {
        export interface Get extends Array < OrderDocument > {}
    }
}