import { DisposalScheduleDayType, DisposalSchedulePeriod } from './DisposalSchedule';

export interface OrderObject {
    ИдентификаторДоговора: string;
    ИдентификаторОбъекта: string;
    ТипОбъекта: string;
    ТипЖилогоДома: string;
    ВидОбъекта: string;
    НаименованиеОбъекта: string;
    АдресОбъекта: string;
    СпособРасчета: string;
    ДатаВключения: string;
    ДатаОтключения: string;
    ТарифнаяЗона: string;
    НормативнаяЗона: string;
    КонтейнернаяПлощадка: string;
    ГрафикОбслуживания: string;
    ГрафикОбслуживанияДанные: {
        Идентификатор: string;
        Периодичность: DisposalSchedulePeriod;
        Дни: {
            [index: number]: {
                ВидДня: DisposalScheduleDayType;
                НомерДня: number;
                Чередовать: boolean;
                КоличествоДней: number;
                ДнейПропуска: number;
            }
        }
        Месяцы: {
            [index: number]: {
                НомерМесяца: number;
                АналогичноПредыдущему: boolean;
            }
        }
    }
}