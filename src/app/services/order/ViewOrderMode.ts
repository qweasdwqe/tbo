export  enum ViewOrderMode {
    Docs = 'Docs',
    Objects = 'Objects',
    Account = 'Account'
}
