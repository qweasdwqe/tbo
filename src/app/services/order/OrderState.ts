import { OrderFull } from './OrderFull';
import { Order } from './Order';
import { OrderObject } from './OrderObject';
import { OrderAccount } from './OrderAccount';
import { OrderDocument } from './OrderDocument';

export interface OrderState {
    isLoaded: boolean;
    order: Order;
    objects: OrderObject[];
    accounts: OrderAccount[];
    documents: OrderDocument[];
}