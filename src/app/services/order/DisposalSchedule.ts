export interface DisposalSchedule {}

export enum DisposalScheduleDayType {
    DayOfWeek = 'ДеньНедели',
        DayOfMonth = 'ДеньМесяца'
}

export enum DisposalSchedulePeriod {
    Month = 'Месяц',
        Week = 'Неделя',
        Year = 'Год'
}

export interface DisposalScheduleDayOfWeek {
    ВидДня: DisposalScheduleDayType;
    НомерДня: number;
    Чередовать: boolean;
    КоличествоДней: number;
    ДнейПропуска: number;
}

export interface DisposalScheduleDayOfMonth {
    ВидДня: DisposalScheduleDayType;
    НомерНедели: number;
    НомерДня: number;
}
