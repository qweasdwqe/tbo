import { OrderObject } from './OrderObject';
import { OrderDocs } from './OrderDocs';
import { OrderAccount } from './OrderAccount';

export interface OrderFull {
    0: OrderAccount[];
    1: OrderObject[];
    2: OrderDocs[];
}

