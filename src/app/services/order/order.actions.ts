import { createAction, props } from '@ngrx/store';
import { OrderFull } from './OrderFull';
import { Order } from './Order';
import { OrderObject } from './OrderObject';
import { OrderAccount } from './OrderAccount';
import { OrderDocument } from './OrderDocument';


function n(value: string) {
    return '[Order] ' + value;
}

export const OrderIsLoading = createAction(n('Order Is Loading'))

export const GetOrder = createAction(n('Get Order'));
export const GetOrderComplete = createAction(
    n('Get Order Complete'),
    props < { payload: Order } > ()
);

export const GetOrderObjects = createAction(n('Get Order Objects'));
export const GetOrderObjectsComplete = createAction(
    n('Get Order Objects Complete'),
    props < { payload: OrderObject[] } > ()
);

export const GetOrderAccounts = createAction(n('Get Order Accounts'));
export const GetOrderAccountsComplete = createAction(
    n('Get Order Accounts Complete'),
    props < { payload: OrderAccount[] } > ()
);

export const GetOrderDocuments = createAction(n('Get Order Documents'));
export const GetOrderDocumentsComplete = createAction(
    n('Get Order Documents Complete'),
    props < { payload: OrderDocument[] } > ()
);

export const InitNewOrder = createAction(n('Init New Order'));
export const EditOrder = createAction(n('Edit Order'));