import { Action, createReducer, on } from '@ngrx/store';
import {
    GetOrderAccountsComplete,
    GetOrderComplete,
    GetOrderDocumentsComplete,
    GetOrderObjectsComplete,
    OrderIsLoading
} from './order.actions';
import { OrderState } from './OrderState';


export const initialState: OrderState = {
    isLoaded: false,
    order: null,
    objects: null,
    accounts: null,
    documents: null
};

export function reducer(STATE: OrderState | undefined, ACTION: Action) {
    return createReducer(
        initialState,

        on(OrderIsLoading, state => ({ ...state, isLoaded: false })),
        on(GetOrderComplete, (state, { payload }) => ({ ...state, order: payload, isLoaded: true })),
        on(GetOrderObjectsComplete, (state, { payload }) => ({ ...state, objects: payload, isLoaded: true })),
        on(GetOrderAccountsComplete, (state, { payload }) => ({ ...state, accounts: payload, isLoaded: true })),
        on(GetOrderDocumentsComplete, (state, { payload }) => ({ ...state, documents: payload, isLoaded: true }))


    )(STATE, ACTION);
}