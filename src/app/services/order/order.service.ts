import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { environment } from 'src/environments/environment';
import { response } from './response';
import { Order } from './Order';
import * as orderActions from './order.actions';


function baseurl(...params) {
    return (environment.resturl + '/' + params.join('/')).replace(/([^:]\/)\/+/g, '$1');
}


@Injectable({
    providedIn: 'root'
})
export class OrderService {
    constructor(
        private http: HttpClient,
        private store: Store < AppState >
    ) {}

    getOrder(id: string) {
        this.store.dispatch(orderActions.OrderIsLoading());
        this.http.get(baseurl('order', id)).subscribe((response: Order) => {
            this.store.dispatch(orderActions.GetOrderComplete({ payload: response }));
        });
    }

    getOrderFull(id: string) {
        this.store.dispatch(orderActions.OrderIsLoading());
        this.http.get(baseurl('orderObjects', id)).subscribe((response: response.order.Get) => {
            const payload = response.debug;
            // this.store.dispatch(GetOrderComplete({ payload }));
        });
    }

    // getOrderAccountDocuments(id: string) {
    //     this.http.get(baseurl('/documents/', id, '?fromdate=20200101&todate20200131'))
    //         .subscribe((response) => {
    //             console.log('РасчетныеДокументы', response);
    //         })
    // }


    getOrderAccounts(id: string) {
        this.store.dispatch(orderActions.OrderIsLoading());
        this.http.get(baseurl('OrderAccount/', id))
            .subscribe((response: response.orderAccounts.Get) => {
                console.log('Взаиморасчеты', response);
                this.store.dispatch(orderActions.GetOrderAccountsComplete({ payload: response }));
            });
    }

    // getOrderAccountAct(id: string) {
    //     this.http.get(baseurl('/Договор/', id, '/АктСверкиВзаиморасчетов?fromdate=20200101&todate20200131'))
    //         .subscribe((response) => {
    //             console.log('АктСверкиВзаиморасчетов', response);
    //         })
    // }


    getOrderDocuments(id: string, start ?: string, end ?: string) {
        const params = (start && end) ? this.getPeriodParams(start, end) : {};

        this.store.dispatch(orderActions.OrderIsLoading());
        this.http.get(baseurl('/documents/', id), { params })
            .subscribe((response: response.orderDocuments.Get) => {
                this.store.dispatch(orderActions.GetOrderDocumentsComplete({ payload: response }));
            });
    }

    getOrderObjects(id: string) {
        this.store.dispatch(orderActions.OrderIsLoading());
        this.http.get(baseurl('OrderAccountObjects', id))
            .subscribe((response: response.orderObjects.Get) => {
                this.store.dispatch(orderActions.GetOrderObjectsComplete({ payload: response }));
            });
    }


    getPeriodParams(start: string, end: string) {
        let _start = '';
        let _end = '';
        const params: any = {};

        let n = new Date(start);
        let y = n.getFullYear().toString();
        let m = Array.from('0' + (n.getMonth() + 1).toString()).reverse().slice(0, 2).reverse().join('');
        let d = n.getDate().toString();
        params.fromdate = y + m + d;

        n = new Date(end);
        y = n.getFullYear().toString();
        m = Array.from('0' + (n.getMonth() + 1).toString()).reverse().slice(0, 2).reverse().join('');
        d = n.getDate().toString();
        params.todate = y + m + d;

        return params;
    }

    // getOrderDocuments(id: string) {
    //     this.http.get(baseurl('documents', id))
    //         .subscribe((response) => {
    //             console.log(response);
    //         });
    // }


}
