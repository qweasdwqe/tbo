export interface OrderDocs {
    id: string;
    title: string;
    status: string;
    address: string;
    areaOfObject: number;
    balance: number;
}