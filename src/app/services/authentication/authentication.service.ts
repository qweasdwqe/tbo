import { Injectable } from '@angular/core';
import { User } from './User';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { response } from './response';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UserService } from '../user/user.service';
import * as userActions from '../user/user.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';

function baseurl(...params) {
    return (environment.resturl + '/' + params.join('/')).replace(/([^:]\/)\/+/g, '$1');
}

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    localStorageName = 'authenticationService';
    user: User;

    constructor(
        private http: HttpClient,
        private router: Router,
        private userService: UserService,
        private store: Store < AppState >
    ) {
        this.user = this.getLocalStorageData();
    }

    isAuth() {
        if (this.user && this.user.token) {
            return true;
        }
        return false;
    }

    getToken() {
        if (this.user) {
            return this.user.token;
        }
        this.logout();
        return null;
    }

    login(username: string, userpass: string) {
        const token: string = 'Basic ' + btoa(username + ':' + userpass);
        const headers: HttpHeaders = new HttpHeaders({ Authorization: token });
        return new Observable < any > (subscriber => {
            this.http
                .post(baseurl('user'), { username, userpass }, { headers })
                .subscribe((response: any) => {
                    if (response.username) {
                        this.user = {
                            username: response.username,
                            token
                        };
                        this.setLocalStorageData(this.user);
                        this.store.dispatch(userActions.SetUserData({ payload: response.user }))
                        subscriber.next(response);
                        this.router.navigate(['/']);
                    } else {
                        subscriber.next(response);
                    }
                    subscriber.complete();
                });
        });
    }

    confirm(user_id: string, code: string) {
        return new Observable < any > (subscriber => {
            this.http
                .post(baseurl('confirm'), { user_id, code }, {  })
                .subscribe((response: any) => {
                    subscriber.next(response);
                    subscriber.complete();
                });
        });
    }

    getLocalStorageData(): User {
        const user: User = JSON.parse(localStorage.getItem(this.localStorageName));
        return user;
    }

    setLocalStorageData(user: User) {
        localStorage.setItem(this.localStorageName, JSON.stringify(user));
    }

    logout() {
        localStorage.removeItem(this.localStorageName);
        this.user = null;
        this.router.navigate(['user', 'login']);
    }
}