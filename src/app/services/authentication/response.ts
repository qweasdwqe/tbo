import { UserData } from '../user/UserData';

export namespace response {
    export interface User {
        username: string;
        user: UserData;
    }
}
