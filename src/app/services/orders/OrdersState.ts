import { ItemOfOrders } from './ItemOfOrders.ts';

export interface OrdersState {
    list: ItemOfOrders[];
    active ?: ItemOfOrders;
}
