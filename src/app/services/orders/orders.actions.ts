import { createAction, props } from '@ngrx/store';
import { response } from './response';
import { ItemOfOrders } from './ItemOfOrders.ts';


function n(value: string) {
    return '[Orders] ' + value;
}


export const GetOrders = createAction(n('Get Orders'));
export const GetOrdersComplete = createAction(n('Get Orders Complete'), props<{payload: ItemOfOrders[]}>());
