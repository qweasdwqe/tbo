import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { GetOrdersComplete } from './orders.actions';
import { response } from './response';
import { ItemOfOrders } from './ItemOfOrders.ts';

function baseurl(...params) {
    return (environment.resturl + '/' + params.join('/')).replace(/([^:]\/)\/+/g, '$1');
}

@Injectable({
    providedIn: 'root'
})
export class OrdersService {
    constructor(
        private http: HttpClient,
        private store: Store < AppState >
    ) {}

    getOrders() {
        this.http.get(baseurl('orders'), {}).subscribe((response: response.orders.Get) => {
            // TODO : replace debug
            const payload: ItemOfOrders[] = [];
            response.forEach(element => {
                payload.push(element.debug[1][0]);
            });

            this.store.dispatch(GetOrdersComplete({ payload }));
        });
    }
}
