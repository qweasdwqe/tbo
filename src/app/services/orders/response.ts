import { ItemOfOrders } from './ItemOfOrders.ts';

// tslint:disable-next-line: no-namespace
export namespace response {
    export namespace orders {
        export interface Get extends Array < any > {
            [index: number]: {
                debug: {
                    [index: number]: {
                        [index: number]: ItemOfOrders;
                    };
                };
            };
        }
    }
}