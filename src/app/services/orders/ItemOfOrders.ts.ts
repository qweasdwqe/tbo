export interface ItemOfOrders {
    'Идентификатор': string;
    'Статус': string;
    'Номер': string;
    'ДатаПодписания': string;
    'ПериодДействия': {
        'ДействуетС': string;
        'ДействуетПо': string;
        'Автопролонгация': boolean;
    };
    'СуммаДоговора': {
        'СФиксированнойСуммой': boolean;
        'СуммаДоговора': boolean;
    };
    'ДатаОстатков': string;
    'СуммаДолга': number;
    'СуммаАванс': number;
    'СуммаСальдо': number;
}
