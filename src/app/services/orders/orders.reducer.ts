import { Action, createReducer, on } from '@ngrx/store';
import { GetOrders, GetOrdersComplete } from './orders.actions';
import { OrdersState } from './OrdersState';


export const initialState: OrdersState = {
    list: null
};

export function reducer(STATE: OrdersState | undefined, ACTION: Action) {
    return createReducer(
        initialState,
        on(GetOrdersComplete, (state, { payload }) => ({ ...state, list: payload })),
    )(STATE, ACTION);
}