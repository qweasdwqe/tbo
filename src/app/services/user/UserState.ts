import { UserData } from './UserData';

export interface UserState {
    organizationName: string;
    userData: UserData;
}
