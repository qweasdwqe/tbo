export interface UserData {
    Идентификатор: string;
    ИНН: string;
    КПП: string;
    ОГРН: string;
    ДатаГосударственнойРегистрации: string;
    ВидДеятельности: string;
    ОКОПФ: {
        Код: string;
        Наименование: string;
    };
    ОсновныеДанные: {
        ТипКонтрагента: string;
        НаименованиеПолное: string;
        НаименованиеСокращенное: string;
    };
}