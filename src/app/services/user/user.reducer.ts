import { Action, createReducer, on } from '@ngrx/store';
import { SetUserData, GetUserData } from './user.actions';
import { UserState } from './UserState';
import { UserService } from './user.service';


export const initialState: UserState = {
    organizationName: null,
    userData: UserService.getUserDataFromLocalStorage()
};

export function reducer(STATE: UserState | undefined, ACTION: Action) {
    return createReducer(
        initialState,
        on(GetUserData, (state, { payload }) => ({ ...state, ...payload })),
        on(SetUserData, (state, { payload }) => {
            UserService.setUserDataToLocalStorage(payload);
            return { ...state, userData: payload };
        })
    )(STATE, ACTION);
}