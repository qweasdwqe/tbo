import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppState } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { response } from './response';
import { environment } from 'src/environments/environment';
import { UserData } from './UserData';
import { Observable } from 'rxjs';

function baseurl(...params) {
    return (environment.resturl + '/' + params.join('/')).replace(/([^:]\/)\/+/g, '$1');
}


@Injectable({
    providedIn: 'root'
})
export class UserService {

    static LOCAL_STORAGE_NAME = 'userServiceData';

    constructor(
        private http: HttpClient,
        private store: Store < AppState >
    ) {}

    static getUserDataFromLocalStorage(): UserData {
        return JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_NAME)) || null;
    }

    static setUserDataToLocalStorage(userdata: UserData) {
        localStorage.setItem(this.LOCAL_STORAGE_NAME, JSON.stringify(userdata));
    }

    signup(inn: string, email: string, password: string, password2: string) {
        return new Observable<any>(subscriber => {
            this.http
                .post(baseurl('signup'), { inn, email, password, password2 })
                .subscribe({
                    next: (response: any) => {
                        subscriber.next(response);
                        subscriber.complete();
                    }, error: () => subscriber.error()
                });
        });
    }

    register(user: any) {
        return new Observable<any>(subscriber => {
            this.http
                .post(baseurl('register'), { user })
                .subscribe({
                    next: (response: any) => {
                        subscriber.next(response);
                        subscriber.complete();
                    }, error: () => subscriber.error()
                });
        });
    }
} 