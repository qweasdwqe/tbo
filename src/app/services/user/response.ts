import { UserData } from './UserData';

export namespace response {
    export namespace user {
        export interface Get {
            user: UserData;
        }
    }
}
