import { createAction, props } from '@ngrx/store';
import { UserData } from './UserData';

function n(value: string) {
    return '[User] ' + value;
}

export const GetUserData = createAction(n('Get User Data'), props < { payload: UserData } > ());
export const SetUserData = createAction(n('Set User Data'), props < { payload: UserData } > ());