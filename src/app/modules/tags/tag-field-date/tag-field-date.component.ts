import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'tag-field-date',
  templateUrl: './tag-field-date.component.html',
  styleUrls: ['./tag-field-date.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class TagFieldDateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
