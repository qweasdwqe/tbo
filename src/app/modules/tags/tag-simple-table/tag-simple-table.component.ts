import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: '[tag-simple-table]',
    templateUrl: './tag-simple-table.component.html',
    styleUrls: ['./tag-simple-table.component.sass'],
    encapsulation: ViewEncapsulation.None
})
export class TagSimpleTableComponent {
  constructor(){
  }
}
