import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
    selector: '[tag-button], [tag-stroked-button]',
    templateUrl: './tag-button.component.html',
    styleUrls: ['./tag-button.component.sass']
})
export class TagButtonComponent {
    constructor(private element: ElementRef<HTMLElement>) {
        this.element.nativeElement.classList.add('tag-button');
    }
}


