import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagButtonComponent } from './tag-button/tag-button.component';
import { TagCardComponent } from './tag-card/tag-card.component';
import { TagFlexComponent } from './tag-flex/tag-flex.component';
import { TagFlexItemComponent } from './tag-flex-item/tag-flex-item.component';
import { TagProgressCircleComponent } from './tag-progress-circle/tag-progress-circle.component';
import { TagSimpleTableComponent } from './tag-simple-table/tag-simple-table.component';
import { TagFieldSelectComponent } from './tag-field-select/tag-field-select.component';
import { TagFieldTextComponent } from './tag-field-text/tag-field-text.component'
import { TagFieldCaptionComponent } from './tag-field-caption/tag-field-caption.component';
import { TagFieldComponent } from './tag-field/tag-field.component';
import { TagFieldHintComponent } from './tag-field-hint/tag-field-hint.component';
import { TagFieldErrorComponent } from './tag-field-error/tag-field-error.component';
import { TagTabsComponent } from './tag-tabs/tag-tabs.component';
import { TagFieldDateComponent } from './tag-field-date/tag-field-date.component';



@NgModule({
    exports: [
        TagButtonComponent,
        TagCardComponent,
        TagFlexComponent,
        TagFlexItemComponent,
        TagProgressCircleComponent,
        TagSimpleTableComponent,
        TagFieldSelectComponent,
        TagFieldCaptionComponent,
        TagFieldComponent,
        TagFieldTextComponent,
        TagFieldHintComponent,
        TagFieldErrorComponent,
        TagTabsComponent,
        TagFieldDateComponent
    ],
    declarations: [
        TagButtonComponent,
        TagCardComponent,
        TagFlexComponent,
        TagFlexItemComponent,
        TagProgressCircleComponent,
        TagSimpleTableComponent,
        TagFieldSelectComponent,
        TagFieldCaptionComponent,
        TagFieldComponent,
        TagFieldTextComponent,
        TagFieldHintComponent,
        TagFieldErrorComponent,
        TagTabsComponent,
        TagFieldDateComponent
    ],
    imports: [
        CommonModule
    ]
})
export class TagsModule {}