import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'tag-field',
  templateUrl: './tag-field.component.html',
  styleUrls: ['./tag-field.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class TagFieldComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
