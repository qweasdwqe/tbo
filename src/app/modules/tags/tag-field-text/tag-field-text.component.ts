import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'tag-field-text',
  templateUrl: './tag-field-text.component.html',
  styleUrls: ['./tag-field-text.component.sass'],
  encapsulation:ViewEncapsulation.None
})
export class TagFieldTextComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
