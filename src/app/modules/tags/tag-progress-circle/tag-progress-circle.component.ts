import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'tag-progress-circle',
    templateUrl: './tag-progress-circle.component.html',
    styleUrls: ['./tag-progress-circle.component.sass']
})
export class TagProgressCircleComponent {}