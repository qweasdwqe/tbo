import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'tag-flex',
    templateUrl: './tag-flex.component.html',
    styleUrls: ['./tag-flex.component.sass'],
    encapsulation: ViewEncapsulation.None
})
export class TagFlexComponent {}