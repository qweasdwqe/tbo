import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'tag-field-caption',
  templateUrl: './tag-field-caption.component.html',
  styleUrls: ['./tag-field-caption.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class TagFieldCaptionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
