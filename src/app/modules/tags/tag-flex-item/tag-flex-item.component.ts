import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'tag-flex-item',
    templateUrl: './tag-flex-item.component.html',
    styleUrls: ['./tag-flex-item.component.sass'],
    encapsulation: ViewEncapsulation.None

})
export class TagFlexItemComponent {}