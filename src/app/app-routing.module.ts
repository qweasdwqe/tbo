import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './components/orders/orders.component';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import { OrderComponent } from './components/order/order.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserSigninComponent } from './components/user-signin/user-signin.component';
import { UserRecoveryComponent } from './components/user-recovery/user-recovery.component';
import { ViewOrderMode } from './services/order/ViewOrderMode';
import { UserEditComponent } from './components/user-edit/user-edit.component';


const routes: Routes = [
    { path: '', redirectTo: '/orders', pathMatch: 'full' },

    { path: 'user/login', component: UserLoginComponent },
    { path: 'user/signin', component: UserSigninComponent },
    { path: 'user/recovery', component: UserRecoveryComponent },
    { path: 'user/edit', component: UserEditComponent },

    { path: 'orders', component: OrdersComponent },
    { path: 'order/new', component: OrderEditComponent },
    { path: 'order/:id', redirectTo: '/orders', pathMatch: 'full' },
    { path: 'order/:id/edit', component: OrderEditComponent },
    { path: 'order/:id/objects', component: OrderComponent, data: { mode: ViewOrderMode.Objects } },
    { path: 'order/:id/docs', component: OrderComponent, data: { mode: ViewOrderMode.Docs } },
    { path: 'order/:id/account', component: OrderComponent, data: { mode: ViewOrderMode.Account } },

    { path: '**', redirectTo: '/orders' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}