import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { metaReducers, reducers } from './app.reducer';
import { HeaderComponent } from './components/header/header.component';
import { OrderEditComponent } from './components/order-edit/order-edit.component';
import { OrderComponent } from './components/order/order.component';
import { OrdersComponent } from './components/orders/orders.component';
import { OrderObjectComponent } from './components/order-object/order-object.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserSigninComponent } from './components/user-signin/user-signin.component';
import { UserRecoveryComponent } from './components/user-recovery/user-recovery.component';
import { JwtInterceptor } from './jwt.interceptor';
import { AuthGuard } from './guards/auth/auth.guard';
import { AuthenticationService } from './services/authentication/authentication.service';
import { TagsModule } from './modules/tags/tags.module';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserSigninForm0Component } from './components/user-signin-form0/user-signin-form0.component';
import { UserSigninForm1Component } from './components/user-signin-form1/user-signin-form1.component';
import { UserSigninForm2Component } from './components/user-signin-form2/user-signin-form2.component';
import { UserSigninForm3Component } from './components/user-signin-form3/user-signin-form3.component';

 
@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        OrderComponent,
        OrdersComponent,
        OrderEditComponent,
        OrderObjectComponent,
        UserLoginComponent,
        UserSigninComponent,
        UserRecoveryComponent,
        UserEditComponent,
        UserSigninForm0Component,
        UserSigninForm1Component,
        UserSigninForm2Component,
        UserSigninForm3Component,
    ],
    imports: [
        TagsModule,
        FormsModule,
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true
            }
        }),
        StoreModule.forRoot(reducers, { metaReducers }),
        EffectsModule.forRoot([]),
    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}