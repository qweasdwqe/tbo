import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication/authentication.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.sass']
})
export class AppComponent {

    constructor(
        private authenticationService: AuthenticationService,
        private location: Location
    ) {
        if (this.location.path().split('?')[0] != "/user/login" && this.authenticationService.isAuth() === false) {
            this.authenticationService.logout();
        }
    }

    isAuth() {
        return this.authenticationService.isAuth();
    }
    back() {
        this.location.back();
    }
}
