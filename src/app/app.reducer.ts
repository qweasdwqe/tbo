import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
} from '@ngrx/store';
import { environment } from '../environments/environment';
import { OrdersState } from './services/orders/OrdersState';
import { reducer as OrdersReducer } from './services/orders/orders.reducer';
import { reducer as OrderReducer } from './services/order/order.reducer';
import { reducer as UserReducer } from './services/user/user.reducer';
import { OrderState } from './services/order/OrderState';
import { UserState } from './services/user/UserState';

export interface AppState {
    orders: OrdersState;
    order: OrderState;
    user: UserState;
}

export const reducers: ActionReducerMap < AppState > = {
    orders: OrdersReducer,
    order: OrderReducer,
    user: UserReducer
};

export function debug(reducer: ActionReducer < any > ): ActionReducer < any > {
    return function(state, action) {
        console.groupCollapsed(action.type);
        console.log(action);
        console.log(state);
        console.groupEnd();
        return reducer(state, action);
    };
}

export const metaReducers: MetaReducer < AppState > [] = !environment.production ? [debug] : [];
