import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { OrderService } from 'src/app/services/order/order.service';
import { UserData } from 'src/app/services/user/UserData';

@Component({
    selector: 'tbo-order-edit',
    templateUrl: './order-edit.component.html',
    styleUrls: ['./order-edit.component.sass']
})
export class OrderEditComponent implements OnInit, OnDestroy {
    storeSubscription: Subscription;

    userData: UserData;

    constructor(
        private route: ActivatedRoute,
        private orderService: OrderService,
        private store: Store < AppState > ,
    ) {}

    ngOnInit() {
        this.storeSubscription = this.store.subscribe((state: AppState) => {
            this.userData = state.user.userData;
        });
    }

    ngOnDestroy() {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    submit() {
        console.log(this.userData);
    }

}
