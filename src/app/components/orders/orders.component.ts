import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { OrdersService } from 'src/app/services/orders/orders.service';
import { Subscription } from 'rxjs';
import { ItemOfOrders } from 'src/app/services/orders/ItemOfOrders.ts';

@Component({
    selector: 'tbo-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.sass']
})
export class OrdersComponent implements OnInit, OnDestroy {
    storeSubscription: Subscription;
    list: ItemOfOrders[] = [];
    LOADING = true;

    constructor(
        private store: Store < AppState > ,
        private ordersService: OrdersService
    ) {}

    ngOnInit() {
        this.ordersService.getOrders();
        this.storeSubscription = this.store.subscribe((data: AppState) => {
            if (data.orders.list) {
                this.LOADING = false;
            } else {
                this.LOADING = true;
            }
            this.list = data.orders.list || [];
        });
    }

    ngOnDestroy() {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }
}
