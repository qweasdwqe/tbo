import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderService } from 'src/app/services/order/order.service';
import { ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/app.reducer';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { OrderObject } from 'src/app/services/order/OrderObject';
import { ViewOrderMode } from 'src/app/services/order/ViewOrderMode';
import { OrderDocs } from 'src/app/services/order/OrderDocs';
import { OrderAccount } from 'src/app/services/order/OrderAccount';
import { OrderDocument } from 'src/app/services/order/OrderDocument';
import { Order } from 'src/app/services/order/Order';

@Component({
    selector: 'tbo-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.sass']
})
export class OrderComponent implements OnInit, OnDestroy {
    isLoaded = false;
    isObjectsMode = false;
    isDocsMode = false;
    isAccountMode = false;

    storeSubscription: Subscription;

    orderInfo: Order;
    orderDocuments: OrderDocument[] = [];
    orderObjects: OrderObject[] = [];
    orderAccounts: OrderAccount[] = [];

    pickedObject: OrderObject | null;
    orderId: string;
    orderNumber: string;

    startDate: string;
    endDate: string;

    constructor(
        private orderService: OrderService,
        private route: ActivatedRoute,
        private store: Store < AppState >
    ) {}

    ngOnInit() {
        this.orderId = this.route.snapshot.paramMap.get('id');
        this.orderService.getOrder(this.orderId);

        switch (this.route.snapshot.data.mode) {
            case ViewOrderMode.Docs:
                this.isDocsMode = true;
                this.orderService.getOrderDocuments(this.orderId);
                break;
            case ViewOrderMode.Objects:
                this.isObjectsMode = true;
                this.orderService.getOrderObjects(this.orderId);
                break;
            case ViewOrderMode.Account:
                this.isAccountMode = true;
                this.orderService.getOrderAccounts(this.orderId);
                break;
            default:
                break;
        }

        this.storeSubscription = this.store.subscribe(data => {
            this.isLoaded = data.order.isLoaded;
            if (data.order.isLoaded) {
                this.orderInfo = data.order.order;
                this.orderAccounts = data.order.accounts || [];
                this.orderObjects = data.order.objects || [];
                this.orderDocuments = data.order.documents || [];
            }
        });
    }

    ngOnDestroy() {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    dateChange() {
        this.orderService.getOrderDocuments(this.orderId, this.startDate, this.endDate);
    }

    pickObject(object: OrderObject) {
        if (this.pickedObject === object) {
            this.pickedObject = null;
        } else {
            this.pickedObject = object;
        }
    }
}
