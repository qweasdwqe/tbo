import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'user-signin-form0',
  templateUrl: './user-signin-form0.component.html',
  styleUrls: ['./user-signin-form0.sass']
})
export class UserSigninForm0Component implements OnInit {
  @Output() onSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();

  inn = '';
  email = '';
  password1 = '';
  password2 = '';
  message = "";
  disabled = false;
  response = {};

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  disable(disabled?: boolean, message?: string) {
    this.message = message;
    this.disabled = disabled;
  }

  submit() {
    this.disable(true);
    this.userService
      .signup(this.inn, this.email, this.password1, this.password2)
      .subscribe({
        next: (response) => { this.disable(false, response.message); this.onSubmit.emit(response); },
        error: () => { this.disable(false, "Произошла ошибка") }
      })
  }
}
