import { Component, OnInit, Input } from '@angular/core';
import { OrderObject } from 'src/app/services/order/OrderObject';
import { DisposalSchedulePeriod } from 'src/app/services/order/DisposalSchedule';

@Component({
    selector: 'tbo-order-object',
    templateUrl: './order-object.component.html',
    styleUrls: ['./order-object.component.sass']
})
export class OrderObjectComponent implements OnInit {
    @Input() object: OrderObject;

    isDisposalSchedulePeriodWeek = false;
    isDisposalSchedulePeriodMonth = false;
    isDisposalSchedulePeriodYear = false;

    ngOnInit() {
        if (this.object && this.object.ГрафикОбслуживанияДанные && this.object.ГрафикОбслуживанияДанные.Периодичность) {
            switch (this.object.ГрафикОбслуживанияДанные.Периодичность) {
                case DisposalSchedulePeriod.Week:
                    this.isDisposalSchedulePeriodWeek = true;
                    break;
                case DisposalSchedulePeriod.Month:
                    this.isDisposalSchedulePeriodMonth = true;
                    break;
                case DisposalSchedulePeriod.Year:
                    this.isDisposalSchedulePeriodYear = true;
                    break;
                default:
                    break;
            }
        }
    }

}