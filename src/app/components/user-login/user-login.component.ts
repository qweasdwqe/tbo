import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'tbo-user-login',
    templateUrl: './user-login.component.html',
    styleUrls: ['./user-login.component.sass']
})
export class UserLoginComponent implements OnInit {

    username = '';
    userpass = '';
    message = "";
    
    constructor(
        private authenticationService: AuthenticationService,
        private route: ActivatedRoute
    ) {

    }

    ngOnInit() {
        const confirm_user_id: string = this.route.snapshot.queryParamMap.get('confirm_user_id');
        const confirm_code: string = this.route.snapshot.queryParamMap.get('confirm_code');
        if (confirm_user_id) { 
            this.authenticationService
            .confirm(confirm_user_id, confirm_code)
            .subscribe(response => { this.message = response.message;})
        }
    }
    submit() {
        this.authenticationService
            .login(this.username, this.userpass)
            .subscribe(response => { this.message = response.message;})
    }
}