import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { SigninForm3 } from 'src/app/services/user/SigninForm3';
import { UserService } from 'src/app/services/user/user.service';

@Component({
    selector: 'user-signin-form3',
    templateUrl: './user-signin-form3.component.html',
    styleUrls: ['./user-signin-form3.component.sass']
})
export class UserSigninForm3Component implements OnInit {
    @Output() onSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Input() user: SigninForm3 = {
        ИНН: '',
        ОГРН: '',
        ПолноеНаименование: '',
        СокращенноеНаименование: '',
        БюджетнаяКоммерческая: '',
        УровеньБюджета: '',
        АдресЮридический: '',
        АдресФактический: '',
        ОКВЭД: '',
        ОКАТО: '',
        ОКТМО: '',
        НомерТелефона: '',
        ФамилияРуководителя: '',
        ИмяРуководителя: '',
        ОтчествоРуководителя: '',
        ПолРуководителя: '',
        ОснованиеДействияРуководителя: '',
        ДолжностьРуководителя: '',
        ИННРуководителя: '',
        ТелефонРуководителя: '',
        БИК: '',
        РасчетныйСчёт: '',
        КорреспондентскийСчёт: '',
        Email: '',
        Пароль: '',
        Пароль2: '',
        ПерсональныеДанныеСогласие: ''
    };
    disabled = false;
    message = "";
    
    constructor(private userService: UserService) {}

    ngOnInit() {}
    
    disable(disabled?: boolean, message?: string) {
        this.message = message;
        this.disabled = disabled;
    }

    submit() {
        this.disable(true);
        this.userService
          .register(this.user)
          .subscribe({
            next: (response) => { this.disable(false, response.message); this.onSubmit.emit(response); },
            error: () => { this.disable(false, "Произошла ошибка") }
        })
    }
}
