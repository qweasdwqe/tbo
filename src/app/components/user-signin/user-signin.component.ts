import { Component, OnInit } from '@angular/core';
import { SigninForm1 } from 'src/app/services/user/SigninForm1';
import { SigninForm2 } from 'src/app/services/user/SigninForm2';
import { SigninForm3 } from 'src/app/services/user/SigninForm3';

@Component({
    selector: 'tbo-user-signin',
    templateUrl: './user-signin.component.html',
    styleUrls: ['./user-signin.component.sass']
})
export class UserSigninComponent implements OnInit {

    mode = 0

    constructor() {
        this.user1 = <SigninForm1>{};
        this.user2 = <SigninForm2>{};
        this.user3 = <SigninForm3>{};
    }

    ngOnInit() {}

    pick(i) {
        this.mode = i
    }

    user1: SigninForm1;
    user2: SigninForm2;
    user3: SigninForm3;

    onSubmit0(response: any) { 
        console.log(response);
        this.user1.ИНН = response.ИНН;
        this.user2.ИНН = response.ИНН;
        this.user3.ИНН = response.ИНН;
        this.user1.Email = response.email;
        this.user2.Email = response.email;
        this.user3.Email = response.email;
        if (response.status == 404) {
            if (response.ТипКонтрагента == 1) {
                this.mode = 3;
                this.user3 = response as SigninForm3;
                if (response.ОсновныеДанные) {
                    this.user3.СокращенноеНаименование = response.ОсновныеДанные.НаименованиеСокращенное;
                    this.user3.ПолноеНаименование = response.ОсновныеДанные.НаименованиеПолное;
                    if (response.Руководитель) { 
                        this.user3.ИНН = response.Руководитель.ИНН;
                        this.user3.ФамилияРуководителя = response.Руководитель.Фамилия;
                        this.user3.ИмяРуководителя = response.Руководитель.Имя;
                        this.user3.ОтчествоРуководителя = response.Руководитель.Отчество;
                        this.user3.ДолжностьРуководителя = response.Руководитель.Должность; 
                        this.user3.ОснованиеДействияРуководителя = response.Руководитель.Основание;
                    }
                }
            } else if (response.ТипКонтрагента == 2) {
                this.mode = 2;
                this.user2 = response as SigninForm2;
            } else { 
                this.mode = 1;
                this.user1 = response as SigninForm1;
            }
        }
    }

    onSubmit1(response: any) {
    }

    onSubmit2(response: any) {
    }

    onSubmit3(response: any) {
    }
}