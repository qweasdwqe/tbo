import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SigninForm1 } from 'src/app/services/user/SigninForm1';
import { UserService } from 'src/app/services/user/user.service';

@Component({
    selector: 'user-signin-form1',
    templateUrl: './user-signin-form1.component.html',
    styleUrls: ['./user-signin-form1.component.sass']
})
export class UserSigninForm1Component implements OnInit {
    @Output() onSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Input()user: SigninForm1 = {
        ИНН: '',
        Фамилия: '',
        Имя: '',
        Отчество: '',
        Пол: '',
        ДатаРождения: '',
        СерияПаспорта: '',
        НомерПаспорта: '',
        ДатаВыдачиПаспорта: '',
        КемВыданПаспорт: '',
        КодПодразделения: '',
        АдресПоПрописке: '',
        АдресФизическогоПроживания: '',
        ОКВЭД: '',
        ОКАТО: '',
        ОКТМО: '',
        НомерТелефона: '',
        ФамилияРуководителя: '',
        ИмяРуководителя: '',
        ОтчествоРуководителя: '',
        ТелефонРуководителя: '',
        EmailРуководителя: '',
        БикБанка: '',
        РасчетныйСчёт: '',
        КорреспондентскийСчёт: '',
        Email: '',
        Пароль: '',
        Пароль2: '',
        ПерсональныеДанныеСогласие: ''
    };

    form = new FormGroup({
        ИНН: new FormControl(this.user.ИНН, [Validators.required]),
        Фамилия: new FormControl(this.user.Фамилия, [Validators.required]),
        Имя: new FormControl(this.user.Имя, [Validators.required]),
        Отчество: new FormControl(this.user.Отчество, [Validators.required]),
        Пол: new FormControl(this.user.Пол, [Validators.required]),
        ДатаРождения: new FormControl(this.user.ДатаРождения, [Validators.required]),
        СерияПаспорта: new FormControl(this.user.СерияПаспорта, [Validators.required]),
        НомерПаспорта: new FormControl(this.user.НомерПаспорта, [Validators.required]),
        ДатаВыдачиПаспорта: new FormControl(this.user.ДатаВыдачиПаспорта, [Validators.required]),
        КемВыданПаспорт: new FormControl(this.user.КемВыданПаспорт, [Validators.required]),
        КодПодразделения: new FormControl(this.user.КодПодразделения, [Validators.required]),
        АдресПоПрописке: new FormControl(this.user.АдресПоПрописке, [Validators.required]),
        АдресФизическогоПроживания: new FormControl(this.user.АдресФизическогоПроживания, [Validators.required]),
        ОКВЭД: new FormControl(this.user.ОКВЭД, [Validators.required]),
        ОКАТО: new FormControl(this.user.ОКАТО, [Validators.required]),
        ОКТМО: new FormControl(this.user.ОКТМО, [Validators.required]),
        НомерТелефона: new FormControl(this.user.НомерТелефона, [Validators.required]),
        ФамилияРуководителя: new FormControl(this.user.ФамилияРуководителя, [Validators.required]),
        ИмяРуководителя: new FormControl(this.user.ИмяРуководителя, [Validators.required]),
        ОтчествоРуководителя: new FormControl(this.user.ОтчествоРуководителя, [Validators.required]),
        ТелефонРуководителя: new FormControl(this.user.ТелефонРуководителя, [Validators.required]),
        EmailРуководителя: new FormControl(this.user.EmailРуководителя, [Validators.required]),
        БикБанка: new FormControl(this.user.БикБанка, [Validators.required]),
        РасчетныйСчёт: new FormControl(this.user.РасчетныйСчёт, [Validators.required]),
        КорреспондентскийСчёт: new FormControl(this.user.КорреспондентскийСчёт, [Validators.required]),
        Email: new FormControl(this.user.Email, [Validators.required]),
        Пароль: new FormControl(this.user.Пароль, [Validators.required]),
        Пароль2: new FormControl(this.user.Пароль2, [Validators.required]),
        ПерсональныеДанныеСогласие: new FormControl(this.user.ПерсональныеДанныеСогласие, [Validators.required]),
    });
    disabled = false;
    message = "";

    constructor(private userService: UserService) {}

    ngOnInit() {}
    
    disable(disabled?: boolean, message?: string) {
        this.message = message;
        this.disabled = disabled;
    }
    
    submit() {
        this.disable(true);
        this.userService
          .register(this.user)
          .subscribe({
            next: (response) => { this.disable(false, response.message); this.onSubmit.emit(response); },
            error: () => { this.disable(false, "Произошла ошибка") }
        })
    }
}
