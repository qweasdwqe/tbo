import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { SigninForm2 } from 'src/app/services/user/SigninForm2';
import { UserService } from 'src/app/services/user/user.service';

@Component({
    selector: 'user-signin-form2',
    templateUrl: './user-signin-form2.component.html',
    styleUrls: ['./user-signin-form2.component.sass']
})
export class UserSigninForm2Component implements OnInit {
    @Output() onSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Input() user: SigninForm2 = {
        ИНН: '',
        ОГРНИП: '',
        ПолноеНаименование: '',
        СокращенноеНаименование: '',
        Фамилия: '',
        Имя: '',
        Отчество: '',
        Пол: '',
        ДатаРождения: '',
        СерияПаспорта: '',
        НомерПаспорта: '',
        ДатаВыдачиПаспорта: '',
        КемВыданПаспорт: '',
        КодПодразделения: '',
        НомерСвидетельства: '',
        ДатаСвидетельства: '',
        АдресЮридический: '',
        АдресФактический: '',
        ОКВЭД: '',
        ОКАТО: '',
        ОКТМО: '',
        НомерТелефона: '',
        ФамилияРуководителя: '',
        ИмяРуководителя: '',
        ОтчествоРуководителя: '',
        ПолРуководителя: '',
        ОснованиеДействияРуководителя: '',
        ДолжностьРуководителя: '',
        ТелефонРуководителя: '',
        EmailРуководителя: '',
        БИК: '',
        РасчетныйСчёт: '',
        КорреспондентскийСчёт: '',
        Email: '',
        Пароль: '',
        Пароль2: '',
        ПерсональныеДанныеСогласие: ''
    };
    disabled = false;
    message = "";

    constructor(private userService: UserService) {}

    ngOnInit() {}
    
    disable(disabled?: boolean, message?: string) {
        this.message = message;
        this.disabled = disabled;
    }
    
    submit() {
        this.disable(true);
        this.userService
          .register(this.user)
          .subscribe({
            next: (response) => { this.disable(false, response.message); this.onSubmit.emit(response); },
            error: () => { this.disable(false, "Произошла ошибка") }
        })
    }
}
