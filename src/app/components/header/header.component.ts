import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/app.reducer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
    selector: 'tbo-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit, OnDestroy {
    organizationName: string;
    storeSubscription: Subscription;
    IS_AUTH = false;

    constructor(
        private store: Store < AppState > ,
        private authenticationService: AuthenticationService,
        private location: Location,
    ) {}

    ngOnInit() {
        this.store.subscribe(state => {
            this.organizationName = state.user.userData.ОсновныеДанные.НаименованиеСокращенное;
        });
    }

    ngOnDestroy() {
        if (this.storeSubscription) {
            this.storeSubscription.unsubscribe();
        }
    }

    isAuth() {
        return this.authenticationService.isAuth();
    }

    logout() {
        return this.authenticationService.logout();
    }

    back() {
        this.location.back();
    }
}
