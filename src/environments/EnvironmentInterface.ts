export interface EnvironmentInterface {
    production: boolean;
    resturl: string;
}
