import { EnvironmentInterface } from './EnvironmentInterface';

export const environment: EnvironmentInterface = {
    production: true,
    resturl: 'http://ecoal03bx.langro.ru/ecoal/'
};
