import { initDB } from './initDB';

const path = require('path');
const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router(initDB);
const middlewares = jsonServer.defaults();


server.use(middlewares);
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
    setTimeout(() => {
        next();
    }, 999);
});

server.use((req, res, next) => {
    if (req.body.username && req.body.userpass) {
        res.json({
            username: 'АБВ Организация',
            token: '2c56ddbb8a4a3d483cc3e25d58a27ecf'
        });
    } else {
        next();
    }
});
server.use(router);

server.listen(3000, () => {
    console.log('JSON Server is running');
});
