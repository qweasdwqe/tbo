import { OrderFull } from '../src/app/services/order/OrderFull';
import { initOrderObject } from './initOrderObject';
import { getRandomInt } from './getRandomInt';
import { initOrderDocs } from './initOrderDocs';

function take(list: any[]) {
    return list[getRandomInt(list.length)];
}

let id = 1;
export function initOrder(): any {
    return {
        id: (id++).toString(),
        date: (Date.now() - (1000 * 60 * 60 * 24 * getRandomInt(365))).toString(),
        status: take(['Status A', 'Status B', 'Status C']),
        numberOfObjects: take([1, 2, 3, 4, 5]),
        balance: take([1500, 2000, 2500]),
        organization: {
            inn: take([123456789, 987654321]).toString(),
            title: take(['АБВ Организация', 'Вектор Организация', 'Типичная Организация']),
            ogrn: take([123456789, 987654321]).toString(),
            legalAddress: take(['Добровольческий переулок', 'Красная Сосна, улица', 'Плеханова, улица']),
            mailingAddress: take(['Добровольческий переулок', 'Красная Сосна, улица', 'Плеханова, улица']),
        },
        bankDetails: {
            bic: take([1, 2, 3, 4, 5]),
            paymentAccount: take([1, 2, 3, 4, 5]),
            nameOfNank: take(['Банк A', 'Банк B', 'Банк C']),
            correspondentAccount: take([123456789, 987654321]).toString(),
            bankAddress: take(['Добровольческий переулок', 'Красная Сосна, улица', 'Плеханова, улица'])
        },
        objects: new Array(getRandomInt(5) + 5).fill(null).map(() => initOrderObject()),
        docs: new Array(getRandomInt(5) + 5).fill(null).map(() => initOrderDocs()), 
    };
}