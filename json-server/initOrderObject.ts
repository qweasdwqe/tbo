import { OrderObject } from '../src/app/services/order/OrderObject';
import { getRandomInt } from './getRandomInt';
import { initOrderDocs } from './initOrderDocs';

function take(list: any[]) {
    return list[getRandomInt(list.length)];
}

export function initOrderObject(): OrderObject {
    return {
        id: take([1, 2, 3, 4, 5]),
        status: take(['Status A', 'Status B', 'Status C']),
        dateCreated: Date.now() - (1000 * 60 * 60 * 24 * getRandomInt(100)),
        object: {
            address: take(['Добровольческий переулок', 'Красная Сосна, улица', 'Плеханова, улица']),
            objectType: take(['типA', 'типB', 'типC']),
            serviceType: take(['По нормам', 'По графику', 'Фиксированный']),
            areaOfObject: take([34.23, 121.21, 56.6]),
            numberOfEmployees: take([10, 20, 30]),
            startTime: Date.now() - (1000 * 60 * 60 * 24 * getRandomInt(10)),
            endTime: Date.now() - (1000 * 60 * 60 * 24 * getRandomInt(20)),
            costPerMonth: take([1500, 2000, 2500]),
            coords: take([
                [55.5, 3.09],
                [59.5, 2.09],
                [65.5, 1.09]
            ]),
        }
    };
}