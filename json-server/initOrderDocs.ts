import { OrderDocs } from '../src/app/services/order/OrderDocs';
import { getRandomInt } from './getRandomInt';

function take(list: any[]) {
    return list[getRandomInt(list.length)];
}

export function initOrderDocs(): OrderDocs {
    return {
        id: take([1, 2, 3, 4, 5]),
        title: take(['Счёт №225', 'Акт №1', 'Счёт №32']),
        status: take(['Status A', 'Status B', 'Status C']),
        address: take(['Добровольческий переулок', 'Красная Сосна, улица', 'Плеханова, улица']),
        areaOfObject: take([34.23, 121.21, 56.6]),
        balance: take([1500, 2000, 2500]),
    }
}