import { initOrderObject } from './initOrderObject';
import { initOrder } from './initOrder';
import { getRandomInt } from './getRandomInt';

const ORDER = new Array(getRandomInt(50) + 10).fill(null).map(() => initOrder())

export const initDB = {
    orders: ORDER,
    objects: [
        new Array(getRandomInt(10) + 50).fill(null).map(() => initOrderObject()),
    ],
    order: ORDER
}